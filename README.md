# README #

Database structure convention
=============================

* **Database name**: redbutton
* **User info table**: Users
* **Request info table** : Requests

User info table
---------------
Example of table:

| id | username | prefix | password |
|----|----------|--------|----------|
|  1 | artsin   | NULL   | artsin   |
|  2 | pavlosh  | NULL   | pavlosh  |
|  3 | xxx      | NULL   | xxx      |

* **id**: MySQL id field
* **username**: username, will be used for authentification on the web portal and MQTT topic connection
* **prefix**: unique prefix for MQTT topic names *maybe it will be not used*. *prefix* field is empty when user registered from frontend and * ***should*** be filled by backend only.
* **password**: password used for frontend and MQTT auth.

Requests info table
-------------------
Example of table:

| id | user_id | start               | end  | time_requested | status  |
|----|---------|---------------------|------|----------------|---------|
|  1 |       1 | 2018-05-03 15:04:05 | NULL | NULL           | waiting |
|  2 |       2 | 2018-05-03 15:07:41 | NULL | NULL           | waiting |
|  3 |       2 | 2018-05-03 16:04:52 | NULL | NULL           | waiting 

* **id**: MySQL id field
* **user_id**: user id from *Users* table.
* **start**: time, when request **is placed in frontend**???
* **end**: time, when request is finished???
* **time_requested**: amount of time(timeslot)
* **status**: enum {'waiting', 'in progress', 'done'}.


MQTT topics convention
======================
