import cv2

from imutils.video import WebcamVideoStream

class VideoCamera(object):
    def __init__(self):
        # Using OpenCV to capture from device 0. If you have trouble capturing
        # from a webcam, comment the line below out and use a video file
        # instead.
        #self.video = cv2.VideoCapture(0)
        cap = WebcamVideoStream(src=0)
        cap.stream.set(cv2.CAP_PROP_FPS, 30)
        cap.stream.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
        cap.stream.set(cv2.CAP_PROP_FRAME_HEIGHT, 1024)
        self.vs = cap.start()
        # If you decide to use video.mp4, you must have this file in the folder
        # as the main.py.
        # self.video = cv2.VideoCapture('video.mp4')
    
    def __del__(self):
        self.vs.stop()
    
    def get_frame(self):
        image = self.vs.read()
        # We are using Motion JPEG, but OpenCV defaults to capture raw images,
        # so we must encode it into JPEG in order to correctly display the
        # video stream.
        return image
