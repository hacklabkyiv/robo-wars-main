local HC = require 'HC'
local MQTT = require 'mqtt'

local SPEED = 40
local REPORT_INTERVAL = 0.1

--local MQTT_SERVER = 'm10.cloudmqtt.com'
--local MQTT_PORT = 16860
--local MQTT_LOGIN = 'wahnfosn'
--local MQTT_PASSWORD = '8P7oll9SD0l5'
--local MQTT_SERVER = 'localhost'
--local MQTT_PORT = 1883
--local MQTT_LOGIN = 'hacklab'
--local MQTT_PASSWORD = 'balkcah'
local MQTT_SERVER = '192.168.1.208'
local MQTT_PORT = 1883
local MQTT_LOGIN = 'emulator'
local MQTT_PASSWORD = 'rotalume'

local walls = {
  HC.rectangle(10, 10, 400, 10),
  HC.rectangle(10, 20, 10, 300),
  HC.rectangle(400, 20, 10, 300),
  HC.rectangle(10, 320, 400, 10),
}
local tank = HC.polygon(0, 0, 0, 20, 50, 20, 50, 0)
local mqtt_client
local STOP, FORWARD, BACK, RIGHT, LEFT = 'stop', 'forward', 'back', 'right', 'left'
local command = STOP
local duration = 0
local progress = 0
local last_report = love.timer.getTime()

function split(str, delim, maxNb)
   -- Eliminate bad cases...
   if string.find(str, delim) == nil then
      return { str }
   end
   if maxNb == nil or maxNb < 1 then
      maxNb = 0    -- No limit
   end
   local result = {}
   local pat = "(.-)" .. delim .. "()"
   local nb = 0
   local lastPos
   for part, pos in string.gfind(str, pat) do
      nb = nb + 1
      result[nb] = part
      lastPos = pos
      if nb == maxNb then
         break
      end
   end
   -- Handle the last field
   if nb ~= maxNb then
      result[nb + 1] = string.sub(str, lastPos)
   end
   return result
end

function callback(topic, payload)
  if topic == "redbutton/command" then
    local commands = split(payload, ';', 10)
    if #commands > 0 then
      local components = split(commands[1], ':')
      command = components[1]
      duration = tonumber(components[2]) / 1000.0
      progress = 0
    end
  end
end

function love.load()
  tank:move(100, 100)
  tank:rotate(math.pi / 6)

  mqtt_client = MQTT.client.create(MQTT_SERVER, MQTT_PORT, callback)
  mqtt_client:auth(MQTT_LOGIN, MQTT_PASSWORD)
  mqtt_client:connect("Love2D Test Client")
  mqtt_client:subscribe({"redbutton/command"})
end

function love.update(dt)
  local reverse_rotation = 0
  local reverse_movement = {0, 0}
  if command ~= STOP then
    progress = progress + dt
    if progress > duration then
      command = STOP
    end
  end
  if command == LEFT then
    tank:rotate(-dt)
    reverse_rotation = dt
  elseif command == RIGHT then
    tank:rotate(dt)
    reverse_rotation = -dt
  elseif command == FORWARD then
    local angle = tank:rotation()
    local dx = dt * SPEED * math.cos(angle)
    local dy = dt * SPEED * math.sin(angle)
    tank:move(dx, dy)
    reverse_movement = {-dx, -dy}
  elseif command == BACK then
    local angle = tank:rotation()
    local dx = dt * SPEED * math.cos(angle)
    local dy = dt * SPEED * math.sin(angle)
    tank:move(-dx, -dy)
    reverse_movement = {dx, dy}
  end
  local collisions = HC.collisions(tank)
  if next(collisions) ~= nil then
    tank:move(unpack(reverse_movement))
    tank:rotate(reverse_rotation)
  end
  local time = love.timer.getTime()
  if time - last_report >= REPORT_INTERVAL then
    local x, y = tank:center()
    local angle = tank:rotation()
    mqtt_client:publish(
        'redbutton/telemetry/position',
         time .. " " .. x .. " " .. y .. " " .. angle)
    last_report = last_report + REPORT_INTERVAL
  end
  mqtt_client:handler()
end

function love.draw()
  for _, wall in ipairs(walls) do
    wall:draw('fill')
  end
  tank:draw('fill')
end
