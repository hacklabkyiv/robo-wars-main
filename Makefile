MARKDOWN = frontend/markdown
GENERATED = frontend/static/generated
all: ${GENERATED}/rules.html ${GENERATED}/python.html 

${GENERATED}/rules.html: ${MARKDOWN}/rules.md
	python3 -m markdown -x toc ${MARKDOWN}/rules.md > ${GENERATED}/rules.html

${GENERATED}/python.html: ${MARKDOWN}/example.py
	highlight -i ${MARKDOWN}/example.py -o ${GENERATED}/python.html --include-style --line-numbers

clean:
	rm ${GENERATED}/*.html
