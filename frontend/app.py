from quart import Quart, websocket, render_template, request, redirect, session, url_for
import database
import asyncio
import json
#from hbmqtt.client import MQTTClient, ClientException
#from hbmqtt.mqtt.constants import QOS_1, QOS_2
import yaml
import os
import sys

app = Quart(__name__)

CONFIG_FILE = "config.yaml"
#CONFIG_FILE = "testconfig_localhost.yml"
db_connector = database.DatabaseConnector(CONFIG_FILE)

CONFIG_FILE = os.path.join(
os.path.dirname(os.path.realpath(__file__)), CONFIG_FILE)

# Initial setup
try:
    with open(CONFIG_FILE, 'r') as yamlfile:
        cfg = yaml.load(yamlfile)
except IOError as e:
    print('[PANIC]', 'config loading', str(e))
    sys.exit(1)

app.config.update({
    'SECRET_KEY': 'development key',
    'USERNAME': 'admin',
    'PASSWORD': 'default',
})


@app.websocket('/events_channel')
async def ws_events_channel():
    while True:
        data = await websocket.receive()
        if data:
            print("Received event webpage: ", data)
        if data == "request time":
            db_connector.request_time(session['username'])
        elif data == "skip request":
            db_connector.skip_request(session['username'])
        await asyncio.sleep(0.1)

@app.websocket('/requests_data')
async def ws_requests_data():
    previous_data = None
    previous_timer_value = None
    while True:
        data = db_connector.dump_json_requests()
        timer_value = db_connector.get_runtimer_value()
        if data != previous_data:
            print("Requests data sent: ", data)
            await websocket.send(data)

        if timer_value != previous_timer_value:
            print("Timer value set", timer_value)
            await websocket.send(timer_value)
        previous_timer_value = timer_value
        previous_data = data
        await asyncio.sleep(0.1)

@app.websocket('/users_data')
async def ws_users_data():
    previous_data = None
    while True:
        #data = db_connector.dump_json_users()
        data = db_connector.get_json_users_data()
        if data != previous_data:
            print("Users data sent: ", data)
            await websocket.send(data)
        previous_data = data
        await asyncio.sleep(0.1)

"""
@app.websocket('/mqtt_data')
async def ws_request_mqtt():
    mqtt_client = MQTTClient()
    connect_string = ("mqtt://%s:%s@%s:%s" % (cfg['mqtt']['username'],
                                              cfg['mqtt']['password'],
                                              cfg['mqtt']['server'],
                                              cfg['mqtt']['port']))
    await mqtt_client.connect(connect_string)
    print(connect_string)
    await mqtt_client.subscribe([
            ('redbutton/telemetry/position', QOS_1),
            ('redbutton/command', QOS_1),
         ])
    while True:
        try:
            message = await mqtt_client.deliver_message()
            packet = message.publish_packet
            print("Mqtt data: %s => %s" % (packet.variable_header.topic_name, str(packet.payload.data)))
            sampledata = {packet.variable_header.topic_name :str(packet.payload.data)}
            await websocket.send(json.dumps(sampledata))
        except ClientException as ce:
            print("Client exception: %s" % ce)
        #await asyncio.sleep(0.1)
"""
@app.route('/', methods=['GET', 'POST'])
async def home():
    """ Session control"""
    if not session.get('logged_in'):
        return redirect(url_for('login'))

    else:
        print(session.get('username'))
        return await render_template('index.html', data=session.get('username'))

@app.route('/emulator', methods=['GET', 'POST'])
async def emulator():
    """ Session control"""
    if not session.get('logged_in'):
        return redirect(url_for('login'))

    else:
        print(session.get('username'))
        data = db_connector.emulator_data(session.get('username'))
        return await render_template('emulator.html', username=data['username'],
                                     prefix=data['prefix'],
                                     password=data['password'])

@app.route('/login', methods=['GET', 'POST'])
async def login():
    """Login Form"""
    if request.method == 'GET':
        return await render_template('login.html')
    else:
        form = await request.form
        name = form['username']
        passw = form['password']
        try:
            data = db_connector.check_user(name, passw)
            print(name, passw)
            if data is not False:
                print("Login successfull")
                session['username'] = name
                session['logged_in'] = True
                return redirect(url_for('home'))
            else:
                return await render_template('login.html', data="Login or password is incorrect")
        except Exception as e:
            return ("Dont Login, exception: %s" % e)

@app.route('/register', methods=['GET', 'POST'])
async def register():
    """Register Form"""
    if request.method == 'POST':
        form = await request.form
        db_connector.add_user(form['username'], form['password'])
        return redirect(url_for('login'))
    else:
        return await render_template('register.html')

@app.route("/logout")
async def logout():
    """Logout Form"""
    session['logged_in'] = False
    return redirect(url_for('home'))

if __name__ == '__main__':
    loop = asyncio.new_event_loop()
    app.debug = True
    app.secret_key = "123"
    app.run(host='0.0.0.0', loop=loop)
