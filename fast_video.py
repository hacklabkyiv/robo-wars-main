# import the necessary packages
from imutils.video import WebcamVideoStream
from imutils.video import FPS
import argparse
import imutils
import cv2
 
# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-v", "--video", type=int, default=0,
        help="Video device to use")
ap.add_argument("-p", "--prefix", type=str, default='screenshot',
        help="Screenshot file name prefix")
ap.add_argument("-hsv", "--hsv", action="store_true",
        help="Whether to convert to HSV")
args = vars(ap.parse_args())

# grab a pointer to the video stream and initialize the FPS counter
print("[INFO] sampling frames from webcam...")
fps = FPS().start()

# created a *threaded* video stream, allow the camera sensor to warmup,
# and start the FPS counter
print("[INFO] sampling THREADED frames from webcam...")
vs = WebcamVideoStream(src=args["video"]).start()
fps = FPS().start()

# loop over some frames...this time using the threaded stream
screenshot = 1
while True:
        # grab the frame from the threaded video stream and resize it
        # to have a maximum width of 400 pixels
        frame = vs.read()

        if args["hsv"]:
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

        cv2.imshow("Frame", frame)
        key = cv2.waitKey(1) & 0xFF

        if key == ord('q'):
            break

        if key == ord('s'):
            filename = args["prefix"] + str(screenshot) + '.png'
            screenshot += 1
            print("[INFO] writing screenshot to " + filename)
            cv2.imwrite(filename, frame)

        # update the FPS counter
        fps.update()

# stop the timer and display FPS information
fps.stop()
print("[INFO] elasped time: {:.2f}".format(fps.elapsed()))
print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))

# do a bit of cleanup
cv2.destroyAllWindows()
vs.stop()
