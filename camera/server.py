#!/usr/bin/python3
"""
   Author: Igor Maculan - n3wtron@gmail.com
   A Simple mjpg stream http server
"""
import cv2
import threading
import http
from http.server import BaseHTTPRequestHandler, HTTPServer
from socketserver import ThreadingMixIn
import time
import sys
import yaml
import os

from imutils.video import WebcamVideoStream
import cv2.aruco as aruco
import numpy as np
import threading
import math

import paho.mqtt.publish as publish
import paho.mqtt.client as mqtt

CONFIG_FILE = 'config.yaml'

class CamHandler(BaseHTTPRequestHandler):
    
    def __init__(self, request, client_address, server):
        img_src = '/cam.mjpg'
        self.html_page = """
            <html>
                <head></head>
                <body>
                    <img src="{}"/>
                </body>
            </html>""".format(img_src)
        self.html_404_page = """
            <html>
                <head></head>
                <body>
                    <h1>NOT FOUND</h1>
                </body>
            </html>"""
        BaseHTTPRequestHandler.__init__(self, request, client_address, server)

    def do_GET(self):
        if self.path.endswith('.mjpg'):
            self.send_response(http.HTTPStatus.OK)
            self.send_header('Content-type', 'multipart/x-mixed-replace; boundary=--jpgboundary')
            self.end_headers()
            while True:
                try:
                    img = self.server.read_frame()
                    retval, jpg = cv2.imencode('.jpg', img)
                    if not retval:
                        raise RuntimeError('Could not encode img to JPEG')
                    jpg_bytes = jpg.tobytes()
                    self.wfile.write("--jpgboundary\r\n".encode())
                    self.send_header('Content-type', 'image/jpeg')
                    self.send_header('Content-length', len(jpg_bytes))
                    self.end_headers()
                    self.wfile.write(jpg_bytes)
                    time.sleep(self.server.read_delay)
                except (IOError, ConnectionError):
                    break
        elif self.path.endswith('.html'):
            self.send_response(http.HTTPStatus.OK)
            self.send_header('Content-type', 'text/html')
            self.end_headers()
            self.wfile.write(self.html_page.encode())
        else:
            self.send_response(http.HTTPStatus.NOT_FOUND)
            self.send_header('Content-type', 'text/html')
            self.end_headers()
            self.wfile.write(self.html_404_page.encode())


class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
    """Handle requests in a separate thread."""
    def __init__(self, cfg, server_address, RequestHandlerClass, bind_and_activate=True):
        HTTPServer.__init__(self, server_address, RequestHandlerClass, bind_and_activate)
        ThreadingMixIn.__init__(self)
        self._capture_path = int(cfg['camera']['src'])
        fps = int(cfg['camera']['fps'])
        self.read_delay = 1. / fps
        self._lock = threading.Lock()
        cam = WebcamVideoStream(src=self._capture_path)
        cam.stream.set(cv2.CAP_PROP_FPS, fps)
        cam.stream.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
        cam.stream.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)
        #cam.stream.set(cv2.CAP_PROP_GAIN, 0)
        #cam.stream.set(cv2.CAP_PROP_BRIGHTNESS, 0.5)
        #cam.stream.set(cv2.CAP_PROP_SATURATION, 0.5)
        #cam.stream.set(cv2.CAP_PROP_HUE, 0.5)
        self._camera = cam.start()
        self._frame = None
        self._cfg = cfg

    def open_video(self):
        pass

    def annotate_video(self):
        aruco_dict = cv2.aruco.getPredefinedDictionary(cv2.aruco.DICT_4X4_50)
        # New coefs for resolution 1280x720
        camera_matrix = np.array(
                [[430.49630481,  0.,        656.93764799],
                 [  0.,        429.92584688,327.5175346 ],
                 [  0.,          0.,          1.        ]],
                dtype='float32')
        dist_coeffs = np.array(
                [[ 0.01208016,-0.04705797, 0.00041252, 0.0012854,  0.01152764]],
                dtype='float32')

        # Old coefs for resolution 640x426
        #camera_matrix = np.array(
        #        [[250.16911135,  0.,        325.68972265],
        #         [  0.,        250.75310099, 233.58285504],
        #         [  0.,          0.,          1.        ]],
        #        dtype='float32')
        #dist_coeffs = np.array(
        #        [[ 0.02708351, -0.05851925, 0.00072606, 0.00046083, 0.01500496]],
        #        dtype='float32')

        points = [
            np.array([(64,62,0), (160,160,0), (160,60,0), (60,60,0)], ('float32')),
            np.array([(52,961,0), (160,860,0), (160,760,0), (60,760,0)], ('float32')),
            np.array([(1268,964,0), (750,460,0), (750,360,0), (650,360,0)], ('float32')),
            np.array([(1265,49,0), (1460,160,0), (1460,60,0), (1360,60,0)], ('float32')),
            np.array([(994,442,0), (1560,860,0), (1560,760,0), (1460,760,0)], ('float32')),
            np.array([(404,107,0), (1160,660,0), (1160,560,0), (1060,560,0)], ('float32'))
            ]
        for point in points:
            point[0] = (point[0][0] + 80 * 0, point[0][1] + 80 * 1, 0)
            point[1] = (point[0][0] + 80 * 1, point[0][1] + 80 * 0, 0)
            point[2] = (point[0][0] + 80 * 1, point[0][1] - 80 * 1, 0)
            point[3] = (point[0][0] + 80 * 0, point[0][1] - 80 * 1, 0)
        ids = np.array([[1], [2], [3], [4], [5], [6]])
        markerLength = 100
        roverLength = 80
        roverId = 0
        board = aruco.Board_create(points, aruco_dict, ids)
        inited = False
        while True:
            orig = self._camera.read().copy()
            corners, ids, rejectedImgPoints = aruco.detectMarkers(orig, aruco_dict)
            retval, rvec, tvec = aruco.estimatePoseBoard(corners, ids, board, camera_matrix, dist_coeffs)
            if retval >= 5:
                inited = True
                rmat, _ = cv2.Rodrigues(rvec)
                homogen = np.vstack((np.hstack((rmat, tvec)),
                                     np.array([0., 0., 0., 1.])))
                camera_to_board = np.linalg.inv(homogen)
                board_rvec = rvec
                board_tvec = tvec
                #pt, _ = cv2.projectPoints(
                #        np.float32([
                #[650,460,0], [750,460,0], [750,360,0], [650,360,0],
                #[1060,660,0], [1160,660,0], [1160,560,0], [1060,560,0]
                #        ]), board_rvec, board_tvec, camera_matrix, dist_coeffs)
                #for i in range(8):
                #    x = int(pt[i][0][0])
                #    y = int(pt[i][0][1])
                #    if abs(x) < 1000000 and abs(y) < 100000:
                #        cv2.circle(orig, (x, y), 3, (255, 0, 0), -1)
            orig = aruco.drawDetectedMarkers(orig, corners, ids, (0,255,0))
            if inited and ids is not None:
                orig = aruco.drawAxis(orig, camera_matrix, dist_coeffs, board_rvec, board_tvec, 50)
                for index, marker in enumerate(ids):
                    if inited and marker == roverId:
                        # Rover
                        rvec, tvec, _ = aruco.estimatePoseSingleMarkers(
                                corners[index], roverLength, camera_matrix, dist_coeffs)
                        rmat, _ = cv2.Rodrigues(rvec)
                        rover_to_camera = np.vstack((np.hstack((rmat,
                                                                tvec[0].transpose())),
                                                     np.array([0., 0., 0., 1.])))
                        position = np.array([[0., 0., 0., 1.],
                                           [roverLength / 2., 0., 0., 1.]]).transpose()
                        found = True
                        res = np.matmul(camera_to_board, np.matmul(rover_to_camera, position))
                        angle = math.atan2(res[1][1] - res[1][0], res[0][1] - res[0][0]) * 180. / math.pi
                        angle = (angle + 90) % 360
                        publish.single('/'.join(['redbutton', 'telemetry', 'position']),
                                payload=' '.join(map(str, [time.time(), res[0][0], res[1][0], angle])),
                                hostname=self._cfg['mqtt']['server'],
                                port=int(self._cfg['mqtt']['port']),
                                auth={'username': self._cfg['mqtt']['username'],
                                      'password': self._cfg['mqtt']['password']},
                                protocol=mqtt.MQTTv311)
                        break

            with self._lock:
                self.frame = orig

    def read_frame(self):
        with self._lock:
            return self.frame

    def serve_forever(self, poll_interval=0.5):
        self.open_video()
        thread = threading.Thread(target=self.annotate_video)
        thread.start()
        try:
            super().serve_forever(poll_interval)
        except KeyboardInterrupt:
            self._camera.stop()


def main():
    # Initial setup
    try:
        with open(os.path.join(os.path.dirname(os.path.realpath(__file__)), CONFIG_FILE), 'r') as yamlfile:
            cfg = yaml.load(yamlfile)
    except IOError as e:
        print("Exception", str(e))
        sys.exit(1)
    server = ThreadedHTTPServer(cfg, ('0.0.0.0', 8080), CamHandler)
    print("server started")
    server.serve_forever()


if __name__ == '__main__':
    main()
