#!/bin/bash

set -x

BASE_PATH="/home/pi/Dev/robo-wars-main"

start_camera() {
	nohup python3 "$BASE_PATH/camera/server.py" >"$BASE_PATH/logs/camera.log" 2>&1 &
	echo $! >"$BASE_PATH/pids/camera.pid"
}

stop_camera() {
	kill $(cat $BASE_PATH/pids/camera.pid)
}

start_backend() {
	nohup python3 "$BASE_PATH/backend/backend.py" >"$BASE_PATH/logs/backend.log" 2>&1 &
	echo $! >"$BASE_PATH/pids/backend.pid"
}

stop_backend() {
	kill $(cat $BASE_PATH/pids/backend.pid)
}

start_frontend() {
	nohup bash -c "source $BASE_PATH/frontend/env/bin/activate && python3 $BASE_PATH/frontend/app.py" >$BASE_PATH/logs/frontend.log 2>&1 &
	echo $! >"$BASE_PATH/pids/frontend.pid"
}

stop_frontend() {
	kill $(cat $BASE_PATH/pids/frontend.pid)
}

case "$1" in
	start) case "$2" in
		  camera) start_camera ;; 
		  backend) start_backend ;;
		  frontend) start_frontend ;;
		  *) start_camera; start_backend; start_frontend ;;
	       esac ;;
	stop) case "$2" in
	          camera) stop_camera ;;
		  backend) stop_backend ;;
		  frontend) stop_frontend ;;
		  *) stop_frontend; stop_backend; stop_camera ;;
	       esac ;;
        cleandb) mysql --user=redbuttonadmin --password=$(cat $BASE_PATH/mysql.password) -e "USE redbutton; DELETE FROM Requests WHERE id > 0;" ;;
	*) echo "redbutton <start <camera | backend | frontend> | cleandb>" ;;
esac
