import argparse
import csv
import cv2

ap = argparse.ArgumentParser()
ap.add_argument("-f", type=str, help="CSV file name.")
ap.add_argument("-hsv", action="store_true", help="Whether to convert to HSV.")
ap.add_argument("-iter", type=int, default=3, help="Number of iterations for erode/dilate.")
ap.add_argument("-lower", type=str, default="0,0,0", help="Lower range.")
ap.add_argument("-upper", type=str, default="255,255,255", help="Lower range.")
args = vars(ap.parse_args())

rangeLower = tuple(map(int, args["lower"].split(",")))
rangeUpper = tuple(map(int, args["upper"].split(",")))

cv2.namedWindow('image', cv2.WINDOW_NORMAL)
data = csv.reader(open(args["f"]))
for line in data:
    x, y, r = map(int, line[1:])
    filename = line[0]
    frame = cv2.imread(filename)
    height, width = frame.shape[:2]
    if args["hsv"]:
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    print(rangeLower, rangeUpper)
    mask = cv2.inRange(frame, rangeLower, rangeUpper)
    cv2.imshow('image', mask)
    cv2.waitKey()
    mask = cv2.erode(mask, None, iterations=args["iter"])
    mask = cv2.dilate(mask, None, iterations=args["iter"])
    cv2.imshow('image', mask)
    cv2.waitKey()
    cnts = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
    if cnts:
        cnts = sorted(cnts, key=cv2.contourArea)
        c = cnts[-1]
        ((cx, cy), cr) = cv2.minEnclosingCircle(c)
        M = cv2.moments(c)
        (mx, my) = (M["m10"] / M["m00"], M["m01"] / M["m00"])
        print(filename, x, y, r, cx, cy, cr, mx, my)
        cv2.circle(frame, (int(cx), int(cy)), int(cr), (0, 255, 0), -1)
    else:
        print(filename, x, y, r, 'N/A', 'N/A', 'N/A', 'N/A', 'N/A')
    cv2.circle(frame, (x, y), r, (255, 255, 255), -1)
    cv2.imshow('image', frame)
    cv2.waitKey()
cv2.destroyAllWindows()
