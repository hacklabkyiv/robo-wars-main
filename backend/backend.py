import time
import sys
import os
import signal
import subprocess

import paho.mqtt.client as mqtt
import pymysql
import yaml

import RPi.GPIO as GPIO

CONNECTING, START, WAITING_ROVER, WAITING_QUEUE, CONFIGURE_BROKER, CONFIGURE_QUEUE, RUNNING = range(7)

GPIO_PIN_BUTTON = 21
GPIO_PIN_LED = 3

def dbConnect(cfg):
    return pymysql.connect(
            host=cfg['mysql']['host'],
            user=cfg['mysql']['user'],
            password=cfg['mysql']['password'],
            db=cfg['mysql']['db'])

def switchState(new):
    global state, state_start
    print('from', state, 'to', new, 'elapsed', time.time() - state_start)
    state_start = time.time()
    state = new

def mqttOnConnect(client, userdata, flags, rc):
    print('[INFO]', 'Connected with code', str(rc))
    client.subscribe('redbutton/telemetry/position')
    client.subscribe('redbutton/command')
    switchState(START)

def mqttOnMessage(client, userdata, msg):
    if msg.topic == 'redbutton/telemetry/position':
        global roverx, rovery, rovera
        _, roverx, rovery, rovera = map(float, msg.payload.split())
    elif msg.topic == 'redbutton/command':
        global last_command
        last_command = time.time()

CONFIG_FILE = os.path.join(
        os.path.dirname(os.path.realpath(__file__)), 'config.yaml')

# Initial setup
try:
    with open(CONFIG_FILE, 'r') as yamlfile:
        cfg = yaml.load(yamlfile)
except IOError as e:
    print('[PANIC]', 'config loading', str(e))
    sys.exit(1)

GPIO.setmode(GPIO.BCM)
GPIO.setup(GPIO_PIN_BUTTON, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(GPIO_PIN_LED, GPIO.OUT)

last_command = None

client = mqtt.Client()
client.on_connect = mqttOnConnect
client.on_message = mqttOnMessage
client.username_pw_set(cfg['mqtt']['username'], cfg['mqtt']['password'])
client.connect(cfg['mqtt']['server'], cfg['mqtt']['port'])

state = CONNECTING
state_start = time.time()
current_user = None

user_check = time.time()

roverx = 0.0
rovery = 0.0
rovera = 0.0

def mosquittoReload(cfg):
    with open(cfg['mosquitto']['pidfile']) as f:
        os.kill(int(f.read()), signal.SIGHUP)

def mosquittoAddUser(cfg, user, password):
    print('[INFO]', 'Adding', user)
    completed = subprocess.run(
            ['mosquitto_passwd', cfg['mosquitto']['passwdfile'], user],
            universal_newlines=True,
            input=((password + "\n")*2))
    if completed.returncode != 0:
        print('[PANIC]', completed)

def mosquittoGenerateACL(cfg, user=None):
    with open(cfg['mosquitto']['aclfile'], 'w') as acl:
        acl.write("""# for the infrastructure
user hacklab
topic readwrite #
user camera
topic write redbutton/telemetry/position
user rover
topic read redbutton/command
user backend
topic read redbutton/telemetry/position
topic read redbutton/command
topic write redbutton/meta
user frontend
topic read redbutton/telemetry/position
topic read redbutton/command
topic read redbutton/meta
""")
        acl.write("\n# for all the users\n")
        connection = dbConnect(cfg)
        try:
            with connection.cursor() as cursor:
                rows = cursor.execute("SELECT username, prefix FROM Users WHERE prefix IS NOT NULL")
                for (username, prefix) in cursor:
                    acl.write("user ")
                    acl.write(username)
                    acl.write("\n")
                    acl.write("topic read redbutton/meta\n")
                    acl.write("topic readwrite redbutton/test/")
                    acl.write(prefix)
                    acl.write("/meta\n")
                    acl.write("topic readwrite redbutton/test/")
                    acl.write(prefix)
                    acl.write("/command\n")
                    acl.write("topic readwrite redbutton/test/")
                    acl.write(prefix)
                    acl.write("/telemetry/position\n")
        finally:
            connection.close()
        if user is not None:
            # request_id, user_id, username, prefix
            username = user[2]
            acl.write("\n# for the current user\n")
            acl.write("user ")
            acl.write(username)
            acl.write("\n")
            acl.write("topic read redbutton/telemetry/position\n")
            acl.write("topic write redbutton/command\n")

def get_button_state():
    if (GPIO.input(GPIO_PIN_BUTTON) == 1):
        return False
    else:
        return True

skip_check = time.time()
while True:

    if time.time() - user_check > 1.0:
        user_check = time.time()
        connection = dbConnect(cfg)
        try:
            with connection.cursor() as cursor:
                rows = cursor.execute("SELECT username, password, id FROM Users WHERE prefix IS NULL")
                for (username, password, user_id) in cursor:
                    mosquittoAddUser(cfg, username, password)
                    cursor.execute("UPDATE Users SET prefix = %s WHERE username = %s", ("u" + str(user_id), username))
                if rows > 0:
                    mosquittoGenerateACL(cfg, current_user)
                    mosquittoReload(cfg)
            connection.commit()
        finally:
            connection.close()

    # =================================
    if state == CONNECTING:
        # Will be changed in mqttOnConnect callback.
        pass
    # =================================
    elif state == START:
        # Check if we have a request in progress and continue in case we do.
        connection = dbConnect(cfg)
        try:
            with connection.cursor() as cursor:
                rows = cursor.execute("SELECT r.id, u.id, u.username, u.prefix, requested FROM Requests r JOIN Users u ON u.id = r.user_id WHERE r.status = 'in progress'")
                if rows == 1:
                    # TODO: re-send the start message to possibly account for failures
                    result = cursor.fetchone()
                    current_user = (result[0], result[1], result[2], result[3])
                    switchState(CONFIGURE_QUEUE)
                    state_start = result[4].timestamp()
                elif rows > 1:
                    print('[PANIC]', 'more than one active request')
                    exit(2)
                else: # rows == 0
                    switchState(WAITING_ROVER)
        finally:
            connection.close()
    # =================================
    elif state == WAITING_ROVER:
        # Check if rover is in the starting position.
        startx = int(cfg['competition']['startx'])
        starty = int(cfg['competition']['starty'])
        starta = int(cfg['competition']['starta'])
        if (abs(roverx - startx) < 15 and
            abs(rovery - starty) < 15 and
            abs(rovera - starta) < 5):
            print(roverx, rovery, rovera, 'HAPPY!!!')
            print('[INFO]', 'Satisfied with rover position', (roverx, rovery, rovera))
            switchState(WAITING_QUEUE)
            GPIO.output(GPIO_PIN_LED, False)
        else:
            print(roverx, rovery, rovera, 'unhappy')
            GPIO.output(GPIO_PIN_LED, True)

    # =================================
    elif state == WAITING_QUEUE:
        if time.time() - state_start > 1.0:
            connection = dbConnect(cfg)
            try:
                with connection.cursor() as cursor:
                    cursor.execute("SELECT r.id, u.id, u.username, u.prefix FROM Requests r JOIN Users u ON u.id = r.user_id WHERE r.status = 'waiting' and u.prefix <> '' ORDER BY r. requested LIMIT 1")
                    result = cursor.fetchone()
                    if result:
                        current_user = (result[0], result[1], result[2], result[3])
                        print('[INFO]', 'Selected request from user', current_user)
                        switchState(CONFIGURE_BROKER)
            finally:
                connection.close()
            state_start = time.time()
    # =================================
    elif state == CONFIGURE_BROKER:
        mosquittoGenerateACL(cfg, current_user)
        mosquittoReload(cfg)
        switchState(CONFIGURE_QUEUE)
    # =================================
    elif state == CONFIGURE_QUEUE:
        connection = dbConnect(cfg)
        try:
            with connection.cursor() as cursor:
                # TODO if we get here after restart - don't update requested with "NOW()", because current user gets extra time.
                cursor.execute("UPDATE Requests SET status = 'in progress', start = NOW() WHERE id = %s", (current_user[0]))
            connection.commit()
        finally:
            connection.close()
        client.publish('redbutton/meta', 'START ' + current_user[2])
        last_command = None
        print('[INFO]', 'Starting slot', current_user)
        switchState(RUNNING)
    # =================================
    elif state == RUNNING:
        if time.time() - skip_check > 1.0:
            skip_check = time.time()
            connection = dbConnect(cfg)
            skip = False
            try:
                with connection.cursor() as cursor:
                    # TODO if we get here after restart - don't update requested with "NOW()", because current user gets extra time.
                    rows = cursor.execute("SELECT status FROM Requests WHERE id = %s", (current_user[0]))
                    if rows != 1:
                        print('[PANIC]', 'no row for active request', current_user[0])
                        exit(3)
                    else: # rows == 1
                        result = cursor.fetchone()
                        if result[0] == 'canceled':
                            skip = True
                connection.commit()
            finally:
                connection.close()
            if not skip:
                last_time = (state_start if last_command is None else last_command)
                if time.time() - last_time > float(cfg['competition']['auto_skip_after_min']) * 60:
                    skip = True
            if skip:
                print('[INFO]', 'Skipped for', current_user)
                client.publish('redbutton/meta', 'SKIPPING')
                client.publish('redbutton/meta', 'STOP ' + current_user[2])
                mosquittoGenerateACL(cfg)
                mosquittoReload(cfg)
                connection = dbConnect(cfg)
                try:
                    with connection.cursor() as cursor:
                        cursor.execute("UPDATE Requests SET status = 'canceled', end = NOW() WHERE id = %s", (current_user[0]))
                    connection.commit()
                finally:
                    connection.close()
                current_user = None
                switchState(START)
        if time.time() - state_start > float(cfg['competition']['slot_duration_min']) * 60:
            print('[INFO]', 'Time is up for player', current_user)
            client.publish('redbutton/meta', 'TIMEOUT')
            client.publish('redbutton/meta', 'STOP ' + current_user[2])
            mosquittoGenerateACL(cfg)
            mosquittoReload(cfg)
            connection = dbConnect(cfg)
            try:
                with connection.cursor() as cursor:
                    cursor.execute("UPDATE Requests SET status = 'timeout', end = NOW() WHERE id = %s", (current_user[0]))
                connection.commit()
            finally:
                connection.close()
            current_user = None
            switchState(START)
        elif get_button_state():
            print('[INFO]', 'Player done', current_user)
            client.publish('redbutton/meta', 'BUTTON')
            client.publish('redbutton/meta', 'STOP ' + current_user[2])
            mosquittoGenerateACL(cfg)
            mosquittoReload(cfg)
            connection = dbConnect(cfg)
            try:
                with connection.cursor() as cursor:
                    cursor.execute("UPDATE Requests SET status = 'success', end = NOW() WHERE id = %s", (current_user[0]))
                connection.commit()
            finally:
                connection.close()
            current_user = None
            switchState(START)
    else:
        print('[PANIC]', 'unknown state', state)
    # =================================
    client.loop(timeout=0.1)
